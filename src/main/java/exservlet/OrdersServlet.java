package exservlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import exservlet.model.Order;

@WebServlet("/api/orders")
public class OrdersServlet extends HttpServlet{

    public static final String ID = "id";

    private List<Order> orders = new ArrayList<>();

    private static final long serialVersionUID = -3137208822950117953L;

    @Override
    protected void doGet(HttpServletRequest req,  HttpServletResponse resp)  throws IOException {
        String ret = "";
        var idString = req.getParameter(ID);
        if(idString != null) {
            try {
                int id = Integer.parseInt(idString);
                if(id > -1 && id < orders.size()) {
                    ret = new ObjectMapper().writeValueAsString(orders.get(id));
                    resp.setHeader("Content-Type", "application/json");
                    resp.setStatus(200);
                }else {
                    ret = "Id not a vaid!";
                    resp.setHeader("Content-Type", "text/plain");

                }
            } catch(Exception e) {
                ret = "Id not a number!";
                resp.setHeader("Content-Type", "text/plain");
            }
        }
        resp.getWriter().print(ret);
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        var reqObj = new ObjectMapper().readValue(req.getInputStream(), Order.class);
        reqObj.setId(Long.valueOf(orders.size()));
        orders.add(reqObj);
        String json = new ObjectMapper().writeValueAsString(reqObj);
        resp.setHeader("Content-Type", "application/json");
        resp.setStatus(200);
        resp.getWriter().print(json);

    }

}