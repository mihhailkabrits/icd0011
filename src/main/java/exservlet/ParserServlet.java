package exservlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;


@WebServlet("/api/parser")
public class ParserServlet extends HttpServlet{

	
	private static final long serialVersionUID = -3137208822950117953L;
	
	@Override
    protected void doGet(HttpServletRequest request,  HttpServletResponse response)  throws IOException {
        response.getWriter().print("Please use POST instead of GET!");
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    	 @SuppressWarnings("unchecked")
		Map<String, String> map = new ObjectMapper().readValue(req.getInputStream(), Map.class);
    
    	var retMap = new HashMap<String, String>();
    	
    	map.entrySet().stream().forEach(entry -> {
            var key = new StringBuilder(entry.getKey());
            var value = new StringBuilder(entry.getValue());
            retMap.put(key.reverse().toString(), value.reverse().toString());
        });
    	
        
        String json = new ObjectMapper().writeValueAsString(retMap);
        resp.setHeader("Content-Type", "application/json");
        resp.setStatus(200);
        resp.getWriter().print(json);
        
    }
	
}